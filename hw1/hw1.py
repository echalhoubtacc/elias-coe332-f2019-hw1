def read_data():
	with open("rainfall.csv", "r") as f:
		lines = f.readlines() #Read data by line into a list
		f.close() #Finished with file, close it
	columns = lines[0].rstrip().split(",") #Remove trailing \n, then separate header into list of columns
	output = []
	for line in lines[1:]: #Iterate over lines, skipping header
		line = line.split(",") #Split line by ","
		output.append({ # Cast each field into appropriate data type and insert in dictionary
			columns[0]: int(line[0]),
			columns[1]: int(line[1]),
			columns[2]: float(line[2])
		})
	return output

def dates(data, start=None, end=None):
	if start == None and end == None: 
		return data #return data if no optional parameters are set

	output = []
	for row in data:
		if start != None and end == None and row["year"] >= start:
			output.append(row)
		elif start == None and end != None and row["year"] <= end:
			output.append(row)
		elif start != None and end != None and row["year"] >= start and row["year"] <= end:
			output.append(row)
	return output

def paginate(data, offset=None, limit=None):
	return data[offset:][:limit]